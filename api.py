import flask
from flask import jsonify
from flask_cors import cross_origin
import requests
from dotenv import load_dotenv
import os

load_dotenv('.env')


app = flask.Flask(__name__)
app.config["DEBUG"] = True
FS_API_URL = os.getenv('FS_API_URL')
HEADERS = {"Token": os.getenv('TOKEN')}


@app.route('/', methods=['GET'])
@cross_origin()
def home():
    return jsonify(message="POST request returned")


@app.route('/api/catalog', methods=['GET'])
@cross_origin()
def catalog():
    products_url = FS_API_URL + '/productos'
    response = requests.get(products_url, headers=HEADERS)

    return jsonify(response.json())


app.run()
